{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where


import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy, noDots, (>->))

import Control.Monad.IO.Class (liftIO)

import qualified Database.PostgreSQL.Simple as Pg
import qualified Database.PostgreSQL.Simple.FromField as Pg
import qualified Database.PostgreSQL.Simple.FromRow as Pg
import qualified Database.PostgreSQL.Simple.ToRow as Pg

import Lucid

import Web.Spock.Safe
import Data.Monoid ((<>))

import Data.Text (pack)
import Data.Text (Text, pack)
import Data.Text.Lazy (toStrict)
import Database.PostgreSQL.Simple.SqlQQ (sql)

import OAuth2.Keys (stravaKey)

--main :: IO ()
--main = 
--  runSpock 8080 $ spockT id $ do
--    get root $
--      html "<a href='/calculator/313/+/3'>Calculate 313 + 3</a>"
--    get ("hello" <//> var) $ \name ->
--      text $ "Hello " <> name <> "!"
--    get ("calculator" <//> var <//> "+" <//> var) $ \a b ->
--      text $ pack $ show (a + b :: Int)


{- postgres connection pool to 'timer' database
-}
dbConn :: PoolOrConn Pg.Connection
dbConn =
  PCConn $
    ConnBuilder
      (Pg.connect
        Pg.defaultConnectInfo 
        { Pg.connectUser      = "timer"
        , Pg.connectPassword  = "timer"
        , Pg.connectDatabase  = "timer"
        }
      )
      Pg.close
      (PoolCfg 5 5 60)


state0 :: ()
state0 = ()



hello :: SpockAction Pg.Connection sess st ()
hello = do
  sid <- getSessionId
  html $ "Hello, <em>" <> sid <> "</em>"


{- FAQ -----------------------------------------------------------------------------------------------------
  type WebStateM conn sess st = WebStateT conn sess st (ResourceT IO)
    newtype ResourceT m a = ResourceT { unResourceT :: I.IORef ReleaseMap -> m a }
      data ReleaseMap = ReleaseMap NextKey RefCount IntMap (ReleaseType -> IO ()) | ReleaseMapClosed
        data ReleaseType = ReleaseEarly | ReleaseNormal | ReleaseException

  type SpockM conn sess st a = SpockT (WebStateM conn sess st) a
  type SpockT = SpockCtxT ()
  newtype SpockCtxT ctx m a
    = SpockCtxT
    { runSpockT :: C.SpockAllT (SafeRouter (ActionT m) ()) (ReaderT (LiftHooked ctx m) m) a
    } deriving (Monad, Functor, Applicative, MonadIO)

  type SpockAction conn sess st = SpockActionCtx () conn sess st
    type SpockActionCtx ctx conn sess st = ActionCtxT ctx (WebStateM conn sess st)
  type ActionT = ActionCtxT ()
  newtype ActionCtxT ctx m a
      = ActionCtxT { runActionCtxT :: ErrorT ActionInterupt (RWST (RequestInfo ctx) () ResponseState m) a }
        deriving (Monad, Functor, Applicative, Alternative, MonadIO, MonadReader (RequestInfo ctx), MonadState ResponseState, MonadError ActionInterupt)

-}
app :: SpockM Pg.Connection sess st ()
app = do

{-
Spock is running on port 8000
GET /
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
  Status: 200 OK 0.0040002s
-}
  -- middleware :: Monad m => Network.Wai.Middleware -> SpockT m ()
  -- middleware :: Monad m => Network.Wai.Middleware -> SpockCtxT () m ()
  middleware logStdoutDev   
  middleware $ staticPolicy (noDots >-> addBase "static")

{-
get :: (Control.Monad.IO.Class.MonadIO m, Data.HVect.HasRep xs) 
    => Path xs                                        ~  Path '[x1,x2,x3]
    -> Data.HVect.HVectElim xs (ActionCtxT ctx m ())  ~  HVectElim '[x1,x2,x3] a1 ≡ x1 -> (x2 -> (x3 -> a1))   ~  a1 ≡ (ActionCtxT ctx m ())
    -> SpockCtxT ctx m ()

get "/"
    :: Control.Monad.IO.Class.MonadIO m
    => ActionCtxT ctx m () 
    -> SpockCtxT ctx m ()
-}
  -- Маршрут по умолч
  get "/" hello
  get ("add" <//> var <//> var) $ \(x :: Int) (y :: Int) -> text ("Sum is: " <> (pack . show) (x + y))
  --get "pg" getPgSettings

main :: IO ()
main = do
  -- runSpock :: Warp.Port -> IO Network.Wai.Middleware -> IO ()
  runSpock
    8000 $ do
      -- spock :: SpockCfg conn sess st -> SpockM conn sess st () -> IO Network.Wai.Middleware
      spock 
        (defaultSpockCfg state0 dbConn ())
        --(SpockCfg state0 dbConn (defaultSessionCfg ()) Nothing)
        app   -- :: SpockM conn sess st ()

  return ()

