@echo off
if not "%~1" == "" (
  if "%~2" == "" (
    lfind . -iregex .*\.hs -exec C:\msys64\usr\bin\pcregrep -Hirnu -C3 --color=always "%~1" {} ;
  )
  if not "%~2" == "" (
    lfind . -iregex .*\.hs -exec C:\msys64\usr\bin\pcregrep -Hirnu -C%~2 --color=always "%~1" {} ;
  )
)