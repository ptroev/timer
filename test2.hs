{-# LANGUAGE KindSignatures #-}   -- Illegal kind signature: ‘[*]’
{-# LANGUAGE DataKinds #-}        -- Illegal kind: ‘[*]’
{-# LANGUAGE TypeOperators #-}    -- Illegal operator ‘:’ in type ‘t : ts’
{-# LANGUAGE GADTs #-}            -- Illegal generalised algebraic data declaration for ‘Vect’
{-# LANGUAGE ViewPatterns #-}     -- Illegal view pattern:  Just -> Just v1

-- pattern guards
patguard m1 m2
  | Just v1 <- m1
  , Just v2 <- m2
  --, False
  = v1 + v2
  | otherwise
  = (-100)

-- ViewPatterns
viewpat (Just -> Just 1) = 100
viewpat (Just -> Just v1) = v1
--clunky2 _ = (-2)

data Vect (ts :: [*]) where
  VNil :: Vect '[]
  (:&:) :: !t -> !(Vect ts) -> Vect (t ': ts)

