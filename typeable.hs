{-# LANGUAGE ExistentialQuantification, GADTs, DeriveDataTypeable #-}

-- Dynamic classification as described in Chapter 34 of "Practical
-- Foundations for Programming Languages"

import Prelude hiding (catch)
import Data.IORef
import Data.Typeable
import System.IO.Unsafe
import qualified Control.Exception as E
import Unsafe.Coerce

-- The arbiter of unicity of symbol names. Bob suggests probabilistic
-- methods would be more practical, but I haven't looked up the
-- cleverness to avoid collisions in that strategy.
{-# NOINLINE globalTagNo #-}
globalTagNo :: IORef Int
globalTagNo = unsafePerformIO (newIORef 0)

-- "Symbols" which don't have values (they're labels) but have types and
-- can allocated freshly in IO.
data Sym a = Sym {-# UNPACK #-} !Int
    deriving Show

-- We could have forced lexical scoping using ST-like trick but we'll
-- play fast and loose for now.  Luckily, GHC will prevent us from
-- committing the polymorphic reference problem.
newsym :: IO (Sym a)
newsym = Sym `fmap` atomicModifyIORef globalTagNo (\n -> (n+1, n))

data Clsfd = forall a. Clsfd (Sym a) a

mkIn :: Sym a -> a -> Clsfd
mkIn a e = Clsfd a e

-- We can do it without the unsafeCoerce (with an explicit value-level
-- GADT witness), but this way is much more convenient!
isIn :: Sym a -> Clsfd -> (a -> r) -> r -> r
isIn (Sym n') (Clsfd (Sym n) e) f z | n == n'   = f (unsafeCoerce e)
                                    | otherwise = z

-- Dynamic classification is a pretty good way to do exception handling

data Exn = Exn Clsfd
    deriving (Typeable)
instance Show Exn where
    show _ = "Exn _" -- boo, maybe the existential should posit Show
instance E.Exception Exn where

throw :: Sym t -> t -> a
throw a e = E.throw (Exn (mkIn a e))

throwIO :: Sym t -> t -> IO a
throwIO a e = E.throwIO (Exn (mkIn a e))

catch :: Sym t -> IO a -> (t -> IO a) -> IO a
catch a m h = E.catch m (\i@(Exn e) -> isIn a e h (E.throwIO i))

data Handler a = Handler (forall t. (Sym t, t -> IO a))

catches :: IO a -> [Handler a] -> IO a
catches io handlers = io `E.catch` catchesHandler handlers

catchesHandler :: [Handler a] -> Exn -> IO a
catchesHandler handlers i@(Exn e) = foldr tryHandler (E.throwIO i) handlers
    where tryHandler (Handler (a, h)) res = isIn a e h res

{-
*Main> :r
[1 of 1] Compiling Main             ( DynClass.hs, interpreted )
Ok, modules loaded: Main.
*Main> x :: Sym Int  y :: Sym Char  z :: Sym Int  catch x (throw x 3) (\(x :: Int) -> print x)
3
*Main> catch x (throw y 'a') (\(x :: Int) -> print x)
*** Exception: Exn _
*Main> catch x (throw z 3) (\(x :: Int) -> print x)
*** Exception: Exn _
-}