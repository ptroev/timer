
git init
git remote add origin https://ptroev@bitbucket.org/ptroev/std1.git
;git add *
git config --global user.name ptroev
git config --global user.email pavel.troev@gmail.com
;git pull origin master
;git pull https://ptroev@bitbucket.org/ptroev/std1.git master
;git push -u origin --all
git status -uno
git commit -a -m "1"
git push ;-u origin
git pull
; git branch --set-upstream-to=origin/master master

; override local changes of given file:
git checkout HEAD^ <file-to-overwrite>
git pull

; git info:
; http://www-cs-students.stanford.edu/~blynn/gitmagic/intl/ru/ch07.html

; remove untracked
git clean -d -fx ""

; remove changed
git stash save --keep-index
git stash drop

; reanimate previous push b4267f to cancel wrong last push
git push -f origin b4267f3367986a633e5ef44e4e702bf28347fad3:master


; http://git-scm.com/book/ru/v1/Ветвление-в-Git-Основы-ветвления-и-слияния
;

; You have not concluded your merge (MERGE_HEAD exists).
; Please, commit your changes before you can merge.
; ------------------
; OK. The problem is your previous pull failed to merge automatically and went to conflict state.
; And the conflict wasn't resolved properly before the next pull.
; 
; 1) Undo the merge and pull again.
;   To undo a merge:
;   git merge --abort [Since git version 1.7.4]
;   git reset --merge [prior git versions]
; 2) Resolve the conflict. (subl conflict-file, git add conflit-file)
; 3) Don't forget to add and commit the merge. (git commit -m "obligatory-comment")
; 4) git pull now should work fine.; 
;


; undo changes in folder
git reset HEAD ./abap/*

; undo last commit:
git reset --soft HEAD~1


Undo a commit and redo

$ git commit ...              (1)
$ git reset --soft HEAD~1     (2)
<< edit files as necessary >> (3)
$ git add ....                (4)
$ git commit -c ORIG_HEAD     (5)
This is what you want to undo
This is most often done when you remembered what you just committed is incomplete, or you misspelled your commit message1, or both. Leaves working tree as it was before "commit".
Make corrections to working tree files.
Stage changes for commit.
Commit the changes, reusing the old commit message. reset copied the old head to .git/ORIG_HEAD; commit with -c ORIG_HEAD will open an editor, which initially contains the log message from the old commit and allows you to edit it. If you do not need to edit the message, you could use the -C option instead.