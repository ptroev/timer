{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE UndecidableInstances #-} -- for ReverseLoop type family


-- dat list = [] | : x xs
-- '[]
-- ':   (infix)

data Vect (ts :: [*]) where
  VNil :: Vect '[]  -- type constructor '[] promoted from value constructor []
  (:&:) :: !t -> !(Vect ts) -> Vect (t ': ts)

type family VectElim (ts :: [*]) (a :: *) :: *
type instance VectElim '[] a = a
type instance VectElim (t ': ts) a = t -> VectElim ts a

data a // b
infixl 0 //

class Extract c e | c -> e where
  extract :: c -> e 

instance Extract (a,b) a where
  extract (x,_) = x

{-
vectFirst :: VectElim ts a -> t
vectFirst (f :&: _) = f
-}


{-instance Show (Rep (ts :: [*])) where
  show RNil = "RNil"
  show (RCons f) = "RCons(" ++ show f ++ ")"
-}