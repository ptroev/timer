@echo off
for /F "usebackq tokens=1,2 delims==" %%i in (`wmic os get LocalDateTime /VALUE 2^>NUL`) do if '.%%i.'=='.LocalDateTime.' set ldt=%%j
set ldt=%ldt:~0,4%-%ldt:~4,2%-%ldt:~6,2% %ldt:~8,2%:%ldt:~10,2%:%ldt:~12,6%

if [%1] == [] (
rem set time2=%time:~-11,2%%time:~-8,2%
rem set time2=%time2: =0%
rem set ts1=%DATE:~6,4%%DATE:~3,2%%DATE:~0,2%-%time2%
set p1=%ldt%
) else (
set p1=%ldt% %1
)
echo %p1%
git add .
git commit -a -m "%p1%"
git push

