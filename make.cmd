@if /i "%~2" == "-p" (
  @set p1=-prof -auto-all
) else (
  @set p1=
)
@rem -fllvm LLVM 2.9
ghc --make -O2 -j2 -rtsopts -threaded %p1% %~1
@if errorlevel 0 ( 
  @del %~1.hi
  @del %~1.o
)
@if not "%~2" == "" (
  @echo abap +RTS -xc
  @echo abap +RTS -p
)