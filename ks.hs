{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}

data Term a where
  K :: Term (x -> y -> x)
  S :: Term ((x -> y -> z) -> ((x -> y) -> (x -> z)))
  Const :: x -> Term x
  (:@) :: Term (x -> y) -> Term x -> Term y

infixl 6 :@

eval :: Term a -> Term a
eval (K :@ x :@ y) = x
eval (S :@ x :@ y :@ z) = x :@ z :@ (y :@ z)
--eval (S :@ x :@ y :@ z) = 
eval others = others


parse :: String -> (forall a. Term a)
parse "K" = K
parse "S" = S
