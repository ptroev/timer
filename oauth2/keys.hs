{-# LANGUAGE OverloadedStrings #-}

module OAuth2.Keys where

import Network.OAuth.OAuth2


stravaKey :: OAuth2
stravaKey = 
  OAuth2
  { oauthClientId             = "11153"
  , oauthClientSecret         = "561cbc5b6c9b802bd17b9067de1de281db38d431"
  , oauthCallback             = Just "http://77.50.176.198:8000"
  --, oauthCallback             = Just "http://sulussport.ru"
  , oauthOAuthorizeEndpoint   = "https://www.strava.com/oauth/authorize"
  , oauthAccessTokenEndpoint  = "https://www.strava.com/oauth/token"
  }

{-
  https://www.strava.com/oauth/authorize?
    client_id=9
    &response_type=code
    &redirect_uri=http://testapp.com/token_exchange
    &scope=write
    &state=mystate
    &approval_prompt=force

  EXAMPLE RESPONSE
  {
    "access_token": "83ebeabdec09f6670863766f792ead24d61fe3f9",
    "athlete": {
      "id": 227615,
      "resource_state": 3,
      "firstname": "John",
      "lastname": "Applestrava",
      "profile_medium": "http://pics.com/227615/medium.jpg",
      "profile": "http://pics.com/227615/large.jpg",
      "city": "San Francisco",
      "state": "California",
      "country": "United States",
      "sex": "M",
      "friend": null,
      "follower": null,
      "premium": true,
      "created_at": "2008-01-01T17:44:00Z",
      "updated_at": "2013-09-04T20:00:50Z",
      "follower_count": 273,
      "friend_count": 19,
      "mutual_friend_count": 0,
      "date_preference": "%m/%d/%Y",
      "measurement_preference": "feet",
      "email": "john@applestrava.com",
      "clubs": [ ],
      "bikes": [ ],
      "shoes": [ ]
    }
  }

-}

{-

-- | http://developer.github.com/v3/oauth/
githubKey :: OAuth2
githubKey = OAuth2 { oauthClientId = "xxxxxxxxxxxxxxx"
                    , oauthClientSecret = "xxxxxxxxxxxxxxxxxxxxxx"
                    , oauthCallback = Just "http://127.0.0.1:9988/githubCallback"
                    , oauthOAuthorizeEndpoint = "https://github.com/login/oauth/authorize"
                    , oauthAccessTokenEndpoint = "https://github.com/login/oauth/access_token"
                    }

-- | oauthCallback = Just "https://developers.google.com/oauthplayground"
googleKey :: OAuth2
googleKey = OAuth2 { oauthClientId = "xxxxxxxxxxxxxxx.apps.googleusercontent.com"
                   , oauthClientSecret = "xxxxxxxxxxxxxxxxxxxxxx"
                   , oauthCallback = Just "http://127.0.0.1:9988/googleCallback"
                   , oauthOAuthorizeEndpoint = "https://accounts.google.com/o/oauth2/auth"
                   , oauthAccessTokenEndpoint = "https://www.googleapis.com/oauth2/v3/token"
                   }

facebookKey :: OAuth2
facebookKey = OAuth2 { oauthClientId = "xxxxxxxxxxxxxxx"
                     , oauthClientSecret = "xxxxxxxxxxxxxxxxxxxxxx"
                     , oauthCallback = Just "http://test.com/cb"
                     , oauthOAuthorizeEndpoint = "https://www.facebook.com/dialog/oauth"
                     , oauthAccessTokenEndpoint = "https://graph.facebook.com/v2.3/oauth/access_token"
                     }

doubanKey :: OAuth2
doubanKey = OAuth2 { oauthClientId = "xxxxxxxxxxxxxxx"
                   , oauthClientSecret = "xxxxxxxxxxxxxxxxxxxxxx"
                   , oauthCallback = Just "http://localhost:9999/oauthCallback"
                   , oauthOAuthorizeEndpoint = "https://www.douban.com/service/auth2/auth"
                   , oauthAccessTokenEndpoint = "https://www.douban.com/service/auth2/token"
                   }

fitbitKey :: OAuth2
fitbitKey = OAuth2 { oauthClientId = "xxxxxx"
                   , oauthClientSecret = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
                   , oauthCallback = Just "http://localhost:9988/oauth2/callback"
                   , oauthOAuthorizeEndpoint = "https://www.fitbit.com/oauth2/authorize"
                   , oauthAccessTokenEndpoint = "https://api.fitbit.com/oauth2/token"
                   }
-}